# Godot Project Manager

## v0.1.18
### Changes
* deprecated godot>location option (use local instead)
* better template support for new command
* changed `local_copy` type to just `copy`
* changed `local_zip` type to just `zip`

## v0.1.14
### Changes
* modify templates to create "tests" folders, not "test" folders
* added test command
### Fixes
* missing options file results in crash

## v0.1.13
### Changes
* 64bit architecture is now default
* added [local](https://gitlab.com/godot-stuff/gs-project-manager/wikis/schema#pull-types) type for assets 
### Fixes
* incorrect path used on linux systems


## v0.1.12
### Changes
* added clean command
* allow path argument on clean, edit and install commands
### Fixes
* assets with subfolders not created properly
* path specified in project not accounted for when editing or runnning
* repository in wrong location with project path specified


## v0.1.11
### Changes
* added godot version 3.1.1
* edit project outside of project.yml location by specifying folder name on command
    ```
    > gspm edit my-project-folder
    ```
### Fixes
* install command failed in some instances
* fix godot 3.1 repository location