import io

__logo__ = """
   __ _ ___ _ __  _ __ ___  
  / _` / __| '_ \| '_ ` _ \ 
 | (_| \__ \ |_) | | | | | |
  \__, |___/ .__/|_| |_| |_|
  |___/    |_|              

"""


__id__ = "gspm"
__name__ = "godot-stuff Project Manager"
__desc__ = "A Command Line Utility to Assist in Managing your GODOT projects."
__copyright__ = "Copyright 2018-2020, SpockerDotNet LLC"
__version__ = '0.1.18'
